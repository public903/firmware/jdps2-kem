#define BAUD 2400
#define MYUBRR F_CPU/16/BAUD-1


void UART_init( unsigned int ubrr);
//------------------------------------------------
void UART_Tx( unsigned char data );
//------------------------------------------------
unsigned char UART_Rx( void );
