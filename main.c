/*
 * main.c
 *
 *  Created on: 14.11.2009
 *      Author: Jat
 */

//ATMEGA8
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include <util/delay.h>
#include "uart.h"
#include "eep_rfid.h"

#define RELE(x) {if(x) PORTC |=(1<<PC0); else PORTC &= ~(1<<PC0);}
#define BEEPER(x) {if(x) PORTC |=(1<<PC1); else PORTC &= ~(1<<PC1);}

//-------------------------PREMENNE-------------------------
uint16_t eeprom_adress=0,eeprom_value;
uint8_t rfid[6]={0x01,0x02,0x03,0x04,0x05,0xff},tmr_beeper,beeper_i,beeper_tmp,tmr_rele;
int8_t USART_tmr=-1;
volatile uint8_t timer0_1ms,timer0_10ms,tmr100ms,tmr_master;

//-------------------------FUNKCIE-------------------------
void avr_init(void);
uint8_t tlacitko_sp(void);
uint8_t input(void);
void over_kartu(void);  // zisti ci prisiel kod karty a hlada v eeprome.Ak najde otvori
void obsluha_beeper(void);       // na zaklade parametrov a prerusenia obsluhuje beeper
void beeper_parametre(uint8_t pocet_pisknuti,uint8_t perioda); // zadanie kolkokrat piskne s periodou x 10ms
void obsluha_rele(void);        // na zaklade parametrov a prerusenia obsluhuje rele
void rele_on(uint8_t perioda_rele); // dlzka zopnutia rele x 100ms
void tlacitko(void);    // obsluha tlacitka na pridavanie master karty

//-------------------------PRERUSENIE-------------------------
ISR(TIMER0_OVF_vect) {		// Obsluha prerusenia - pretecenie TIMER0
	TCNT0=0x64;				// prerusenie kazdych 1ms
	if (++timer0_1ms>=10) {	// kazdu milisekundu pripocitaj timer0_1ms
	  timer0_1ms=0;        // 10ms
	  if (beeper_tmp>0) --beeper_tmp;
	  obsluha_beeper();
	  if (++timer0_10ms>=10) {
	    timer0_10ms=0;             // 100ms
	    ++tmr100ms;       // kazdych 100 milisekun pripocitaj timer0_100ms
	    if (tmr_rele!=0) --tmr_rele;
	    obsluha_rele();
	    if (tmr_master > 0) tmr_master--;  // casovac cakania na kod karty
	  }
	}
        if (USART_tmr>=0) USART_tmr++;  // meranie casu medzi 2 rx bajtami
}
//------------------------USART-PRERUSENIE--------------------------------
ISR(USART_RXC_vect) {
  rfid[5]=rfid[4];
  rfid[4]=rfid[3];
  rfid[3]=rfid[2];
  rfid[2]=rfid[1];
  rfid[1]=rfid[0];
  rfid[0]=UDR;
  USART_tmr=0;         // nuluj casovac prichodu bajtu do buffra-startuj meranie
}


//-------------------------MAIN-------------------------
int main(void)
{
avr_init();
sei();
UART_init ( MYUBRR );

        while(1)
	{
	  over_kartu();
	  if (tlacitko_sp()) tlacitko();
	}
}


//---------------------------------------------------------------
//---------FUNKCIE-----------------------------------------------
//---------------------------------------------------------------

void avr_init(void) {
	PORTC=(0<<PC0 | 0<<PC1 | 1<<PC2 | 1<<PC3);
	DDRC=(1<<PC0 | 1<<PC1 | 0<<PC2 | 0<<PC3);

	// Timer/Counter 0 initialization
	// Clock source: System Clock
	// Clock value: 156,000 kHz
	TCCR0=0x03;
	TCNT0=0x64;

	// Timer(s)/Counter(s) Interrupt(s) initialization
	TIMSK=0x01;

}
//---------------------------------------------------------------
uint8_t tlacitko_sp(void) {
  uint8_t tl_sp;
  tl_sp=(PINC & 1<<PC3);
  return(!(tl_sp && 1<<PC3));
}
//---------------------------------------------------------------
uint8_t input(void) {
  uint8_t in;
  in=(PINC & 1<<PC2);
  return(in && 1<<PC2);
}
//---------------------------------------------------------------
void master_karta(void) { // obsluha prijatej master karty
uint16_t mkea;
  beeper_parametre(3,20);
  tmr_master=100;        // 10s cakaj na kartu
  do {
    if (USART_tmr>=10) { // prisiel packet
      USART_tmr=-1;
      eeprom_value=eep_find_value(&rfid[0]);

      switch (eeprom_value) {
      case 10000 :
              mkea=eep_free_adress();    // karta sa nenachadza v pamati
              eep_write_value(mkea,(uint8_t *)rfid);       // zapis novej karty
              beeper_parametre(1,20);
              tmr_master=0;
              break;
      case 0 :
              tmr_master=0;           // master karta-ukonci edit rezim
              break;
      default :
              rfid[0]=0xff;rfid[1]=0xff;rfid[2]=0xff;rfid[3]=0xff;rfid[4]=0xff;rfid[5]=0xff;
              eep_write_value(eeprom_value,(uint8_t *)rfid);  // karta sa nachadza v pamati-vymaz kartu
              beeper_parametre(2,10);
              tmr_master=0;
              break;
      }
    }

  } while(tmr_master>0);    // cakaj na packet (kartu) 5s
}
//---------------------------------------------------------------
void over_kartu(void) {  // zisti ci prisiel kod karty a hlada v eeprome.Ak najde otvori
      if (USART_tmr>=10) { // prisiel packet
      USART_tmr=-1;
      eeprom_value=eep_find_value(&rfid[0]);

      switch (eeprom_value) {
      case 10000 :
              RELE(0);                // neplatna karta
              beeper_parametre(2,10);
              break;
      case 0 :
              master_karta();           // master karta
              break;
      default :
              rele_on(30);                // platna karta
              beeper_parametre(1,20);
              break;
      }

    }
}
//---------------------------------------------------------------
void obsluha_beeper(void) {
  if (beeper_i>0) {
    if (beeper_tmp==0) {
      beeper_tmp=tmr_beeper;
      if (beeper_i % 2 == 0) BEEPER(1)
      else BEEPER(0);
      --beeper_i;
    }
  }
}
//---------------------------------------------------------------
void beeper_parametre(uint8_t pocet_pisknuti,uint8_t perioda) { // zadanie kolkokrat piskne s periodou x 100ms
  beeper_i=pocet_pisknuti*2;
  tmr_beeper=perioda;
}
//---------------------------------------------------------------
void obsluha_rele(void) {
    if (tmr_rele==0) {
      RELE(0);
    }
}
//---------------------------------------------------------------
void rele_on(uint8_t perioda_rele) { // dlzka zopnutia rele x 100ms
  tmr_rele=perioda_rele;
  RELE(1);
}
//---------------------------------------------------------------
void tlacitko(void) { // obsluha tlacitka na pridavanie master karty
    uint16_t tl_eep_value=0xffff,tl_i;
    beeper_parametre(3,20);
    _delay_ms(10);
    tmr_master=100;        // 10s cakaj na kartu
    do {
      if (USART_tmr>=10) { // prisiel packet
        USART_tmr=-1;

                rfid[5]=0xff;
                eep_write_value(0,(uint8_t *)rfid);       // zapis novej master karty
                beeper_parametre(3,20);
                tmr_master=0;
      }
    } while(tmr_master>0);    // cakaj na packet (kartu) 10s
    if (tlacitko_sp()) {
      for (tl_i=0;tl_i<511;tl_i=tl_i+2) eeprom_write_word((uint16_t *)tl_i,tl_eep_value);
      beeper_parametre(1,50);
    };
    while(tlacitko_sp());
    _delay_ms(10);
  }
