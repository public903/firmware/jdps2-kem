# JDPS2

#### Parametre
**Napájanie:** 12V/100mA bez zámku  
**Čítačka:** TRD80 alebo Kascomp RFID_EM  
**Vstupy:** Rx - UART TTL pre čítačku, IN - digitálny vstup 5V  
**Výstupy:**  

- Indikátor: 
	- B - buzzer
	- G - LED green
	- R - LED red
- Zámok: 12V / 1A max

**Max. prístupových kariet:** 100  
**Max. MASTER kariet:** 1  

#### Vlastnosti:
Jednodverový prístupový systém JDPS2 je modul ktorý slúži na riadenie prístupu osôb na základe platnej RFID karty. Riadi jeden vstup/výstup, resp dvere, bráničku a pod., pomocou elektrického zámku. Modul má vnútornú pamäť EEPROM do ktorej ukladá prístupové karty ako aj kartu MASTER. Prístupové karty sa do modulu pridávajú pomocou MASTER karty. MASTER karta sa pridáva pomocou tlačítka.

#### Vymazanie celej pamäti:
- podržať tlačítko na cca 5s, až kým nezaznie dlhý zvukový signál
#### Pridávanie MASTER karty:
- stlačiť tlačítko
- priložiť kartu - v tomto momente sa stane z akejkolvek karty, MASTER karta
#### Pridanie prístupovej karty:
- priložiť MASTER kartu
- priložiť kartu - v tomto momente sa stane z akejkolvek karty, prístupová
#### Vymazanie prístupovej karty:
- priložiť MASTER kartu
- priložiť prístupovú kartu - v tomto momente sa z pamäti vymaže