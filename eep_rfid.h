/*
	eep_rfid.h

	Praca s rfid 5-bytovym kodom v EEPROMKE

	Trnik	30.11.09
*/


int16_t eep_free_adress(void);  // vrati najblizsiu volnu adresu v eepromke

int16_t eep_find_value(uint8_t *efv_hodnota );   // vrati adresu na ktorej sa nachadza hodnota

void eep_write_value(uint16_t ewv_adress,uint8_t *ewv_hodnota );   // zapise na adresu pole 5 bajtov
