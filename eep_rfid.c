/*
	eep_rfid.c

	Praca s rfid 5-bytovym kodom v EEPROMKE

	Trnik	30.11.09
*/
#include <avr/eeprom.h>


int16_t eep_free_adress(void) {  // vrati najblizsiu volnu adresu v eepromke
  #define start_adress 6
  uint8_t efa_tmp=0,efa_offset=0;
  int16_t efa_eeprom_adress=start_adress;

  while(511 - efa_eeprom_adress >= start_adress) {
    if (0xffff == eeprom_read_word((uint16_t *)efa_eeprom_adress + efa_offset)) {
      if (++efa_tmp >= 3) return(efa_eeprom_adress);
      if ((efa_offset = efa_offset + 2) >= 6) {
        efa_offset=0;
        efa_eeprom_adress = efa_eeprom_adress + 6;
        efa_tmp=0;
      }
    }
    else {
      efa_eeprom_adress = efa_eeprom_adress + 6;
      efa_offset = 0;
      efa_tmp = 0;
    }
  }
  return(-1);   // pamat je plna
}

//---------------------------------------------------------------

int16_t eep_find_value(uint8_t *efv_hodnota )
{  // vrati adresu na ktorej sa nachadza hodnota
  #define start_adress_find 0
  uint8_t efv_tmp=0,efv_offset=0;
  uint16_t efv_eeprom_adress=start_adress_find;
  uint16_t efv_eeprom_value;
  efv_hodnota[5]=0xff;

  while(efv_eeprom_adress < 510) {
    efv_eeprom_value=eeprom_read_word((uint16_t *)efv_eeprom_adress + efv_offset);
    if ((((uint8_t *)&efv_eeprom_value)[0] == ((uint8_t *)efv_hodnota)[efv_offset*2]) && (((uint8_t *)&efv_eeprom_value)[1] == ((uint8_t *)efv_hodnota)[efv_offset*2+1])) {
      if (++efv_tmp >= 3) return(efv_eeprom_adress);
      if ((efv_offset = efv_offset + 1) >= 3) {
        efv_offset=0;
        efv_eeprom_adress = efv_eeprom_adress + 3;
        efv_tmp=0;
      }
    }
    else {
      efv_eeprom_adress = efv_eeprom_adress + 3;
      efv_offset = 0;
      efv_tmp = 0;
    }
  }
  return(10000);   // hodnota nenajdena
}

//---------------------------------------------------------------

void eep_write_value(uint16_t ewv_adress, uint8_t *ewv_hodnota )
{  // zapise na adresu pole 5 bajtov
  uint16_t ewv_value;
  uint8_t ewv_i;

  for (ewv_i=0;ewv_i<3;ewv_i++) {
    ((uint8_t *)&ewv_value)[0]=((uint8_t *)ewv_hodnota)[ewv_i*2];
    ((uint8_t *)&ewv_value)[1]=((uint8_t *)ewv_hodnota)[ewv_i*2+1];
    do {} while (!eeprom_is_ready());
    eeprom_write_word((uint16_t *)ewv_adress,ewv_value);
    ewv_adress=ewv_adress+2;
  }
return;
}
